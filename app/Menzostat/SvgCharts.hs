-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2023 David Koňařík
{-# LANGUAGE DuplicateRecordFields, ImportQualifiedPost, OverloadedRecordDot,
             OverloadedStrings, QuasiQuotes #-}
module Menzostat.SvgCharts where

import Data.Function
import Data.List
import Data.Maybe
import Data.Text qualified as T
import Formatting
import Lucid.Svg
import Text.RawString.QQ
import Control.Monad (when)

data Axis = XAxis | YAxis

-- Helper functions for 2-tuple pointwise operations
(.+) :: (Num a, Num b) => (a, b) -> (a, b) -> (a, b)
(a1, a2) .+ (b1, b2) = (a1 + b1, a2 + b2)

(.-) :: (Num a, Num b) => (a, b) -> (a, b) -> (a, b)
(a1, a2) .- (b1, b2) = (a1 - b1, a2 - b2)

(.*) :: (Num a) => a -> (a, a) -> (a, a)
a .* (b1, b2) = (a * b1, a * b2)

-- Scales, helpers for positioning data based on values

data Scale a = Scale {
    scale :: a -> Maybe Double,
    min :: a,
    max :: a }
data XYScale a b = XYScale {
    scale :: (a, b) -> Maybe (Double, Double),
    min :: (a, b),
    max :: (a, b) }

(<+>) :: Scale a -> Scale Double -> Scale a
a <+> b =
    let xmin = fromJust $ a.scale a.min
        xmax = fromJust $ a.scale a.max in
    Scale {
        scale = \v -> do
            x <- a.scale v
            b.scale ((x - xmin)/(xmax - xmin) * (b.max - b.min) + b.min),
        -- We rely on b being a straightforward mapping
        min = a.min, max = a.max
    }

(<++>) :: XYScale a b -> XYScale Double Double -> XYScale a b
a <++> b =
    let (xmin, ymin) = fromJust $ a.scale a.min
        (xmax, ymax) = fromJust $ a.scale a.max
        (bxmin, bymin) = b.min
        (bxmax, bymax) = b.max in
    XYScale {
        scale = \v -> do
            (x, y) <- a.scale v
            b.scale (
                (x - xmin)/(xmax - xmin) * (bxmax - bxmin) + bxmin,
                (y - ymin)/(ymax - ymin) * (bymax - bymin) + bymin),
        -- We rely on b being a straightforward mapping
        min = a.min, max = a.max
    }

categoryScale :: (Eq a) => [a] -> Scale a
categoryScale cats = Scale {
    scale = \v -> do
        idx <- elemIndex v cats
        pure $ (1 / fromIntegral (length cats)) * fromIntegral idx,
    min = head cats,
    max = last cats
}

doubleScale :: Double -> Double -> Scale Double
doubleScale min max = Scale {
    scale = \v ->
        if min <= v && v <= max then Just $ (v - min)/(max - min)
        else Nothing,
    min = min,
    max = max
}

combinedXYScale :: Scale a -> Scale b -> XYScale a b
combinedXYScale xs ys = XYScale {
    scale = \(xv, yv) -> do
        x <- xs.scale xv
        y <- ys.scale yv
        pure (x, y),
    min = (xs.min, ys.min),
    max = (xs.max, ys.max)
}

paddedXYScale :: Double -> Double -> Double -> Double -> XYScale a b -> XYScale a b
paddedXYScale left right top bottom base =
    let (xmin, ymin) = fromJust $ base.scale base.min
        (xmax, ymax) = fromJust $ base.scale base.max
        xRange = abs (xmax - xmin)
        xCoeff = (xRange - left - right)/xRange
        yRange = abs (ymax - ymin)
        yCoeff = (yRange - top - bottom)/yRange in
    XYScale {
        scale = \v -> base.scale v
                    & fmap (\(x, y) -> (x*xCoeff + left,
                                        y*yCoeff + top)),
        min = base.min,
        max = base.max
    }


-- Creates a rectangle where the Y axis points up (convenient for data)
svgXYScale :: Double -> Double -> XYScale Double Double
svgXYScale width height =
    XYScale {
        scale = \(x, y) ->
            if x >= 0 && x <= width
            && y >= 0 && y <= height
            then Just (x, height - y)
            else Nothing,
        min = (0, 0),
        max = (width, height)
    }

-- Helper function for creating SVG elements from double coordinates

-- Show Double
sd :: Double -> T.Text
sd = T.pack . show

dRect :: (Double, Double) -> Double -> Double -> [Attribute] -> Svg ()
dRect (x, y) w h attrs =
    rect_ $ [x_ $ sd x, y_ $ sd y, width_ $ sd w, height_ $ sd h] ++ attrs

dPolyLine :: [(Double, Double)] -> [Attribute] -> Svg ()
dPolyLine pts attrs = do
    let ptsStr =
         T.intercalate " "
         $ map (\(x,y) -> sformat (float%","%float) x y) pts
    when (pts /= []) $ polyline_ $ points_ ptsStr : attrs

dText :: (Double, Double) -> T.Text -> [Attribute] -> Svg ()
dText (x, y) txt attrs =
    text_ ([x_ $ sd x, y_ $ sd y] ++ attrs) (toHtml txt)

axisLine :: Axis -> XYScale a b -> Svg ()
axisLine axis scale = do
    let (x0, y0) = scale.min
    let (xm, ym) = scale.max
    let endpt = case axis of XAxis -> (xm, y0)
                             YAxis -> (x0, ym)
    dPolyLine [fromJust (scale.scale (x0, y0)),
               fromJust (scale.scale endpt)]
              [class_ "xaxis"]

xAxis :: XYScale a b -> Svg ()
xAxis = axisLine XAxis

yAxis :: XYScale a b -> Svg ()
yAxis = axisLine YAxis

axisTicks :: T.Text -> (Double, Double) -> (a -> (Double, Double))
          -> Double -> [(a, T.Text)] -> Svg ()
axisTicks cls otherUnit scale rot ticks =
    ticks & mapM_ (\(i, txt) -> do
        let (x, y) = scale i
        dPolyLine
            [(x, y) .- (2.*otherUnit), (x, y) .+ (10.*otherUnit)]
            [class_ cls]
        let (textX, textY) = (x, y) .+ (15.*otherUnit)
        dText (textX, textY) txt
              [class_ cls,
               -- Ideally this would be done with CSS, but rotating the text
               -- along its anchor requires knowledge of that anchor's
               -- position.
               transform_ (sformat ("rotate("%float%", "%float%", "%float%")")
                                   rot textX textY)])

xTicks :: XYScale a b -> Double -> [(a, T.Text)] -> Svg ()
xTicks scale =
    let xscale x = (fromJust $ scale.scale (x, snd scale.min)) in
    axisTicks "xtick" (0, 1) xscale

yTicks :: XYScale a b -> Double -> [(b, T.Text)] -> Svg ()
yTicks scale =
    let yscale y = (fromJust $ scale.scale (fst scale.min, y)) in
    axisTicks "ytick" (-1, 0) yscale

vertBar :: XYScale x y -> y -> Double -> x -> y -> [Attribute] -> Svg ()
vertBar cs y0 width x y attrs = do
     let (bxb, byb) = fromJust $ cs.scale (x, y0)
     let (_, byt) = fromJust $ cs.scale (x, y)
     let bx1 = bxb - width/2

     dRect (bx1, byt) width (byb-byt) attrs

simpleChartBase :: Double -> Double -> T.Text
                -> (XYScale Double Double -> Svg ())
                -> Svg ()
simpleChartBase width height extraCss content = do
    doctype_
    with svg11_
        [version_ "1.1",
         width_ $ sd width,
         height_ $ sd height] $ do
        style_ $ toHtml $ baseCss <> extraCss
        content $ svgXYScale width height

baseCss :: T.Text
baseCss = [r|
svg {
    font-family: sans-serif;
}

text.xtick {
    text-anchor: middle;
    dominant-baseline: hanging;
}
text.ytick {
    text-anchor: end;
    dominant-baseline: central;
}
polyline.xtick, polyline.xaxis, polyline.ytick, polyline.yaxis {
    fill: none;
    stroke: #555;
    stroke-width: 4;
}
polyline.data1 {
    stroke-width: 4px;
    stroke: coral;
    stroke-linejoin: round;
    fill: none;
}
polyline.data2 {
    stroke-width: 4px;
    stroke: steelblue;
    stroke-linejoin: round;
    fill: none;
}
rect.data1 {
    fill: coral;
}
rect.data2 {
    fill: steelblue;
}
|]
