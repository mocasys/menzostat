-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2023 David Koňařík
{-# LANGUAGE DataKinds, DeriveGeneric, DuplicateRecordFields,
             ImportQualifiedPost, OverloadedStrings #-}

module Menzostat.WebkreditClient where

import Control.Monad.IO.Class
import Data.Aeson
import Data.Aeson.KeyMap qualified as KeyMap
import Data.Aeson.Types
import Data.Scientific
import Data.Text
import Data.Time.Clock
import GHC.Generics
import Network.HTTP.Req

import Menzostat.Utils
import Data.Time
import Data.Time.Format.ISO8601

data Canteen = Canteen {
    id :: Int,
    name :: Text,
    code :: Text
} deriving (Show, Generic)
instance FromJSON Canteen

data Meal = Meal {
    altId :: Int,
    mealKindId :: Int,
    mealKindName :: Text,
    mealName :: Text,
    price :: Scientific,
    price2 :: Scientific,
    currency :: Text,
    countAvailable :: Maybe Int,
    countOrdered :: Int,
    allergens :: Maybe Text,
    pictograms :: Maybe [Int]
} deriving (Show, Generic)
instance FromJSON Meal

newtype MealRow = MealRow {
    item :: Meal
} deriving (Show, Generic)
instance FromJSON MealRow

data MenuGroup = MenuGroup {
    -- Do not use! Has new wonky timezone issues
    date :: UTCTime,
    mealKindId :: Int,
    mealKindName :: Maybe Text,
    rows :: Maybe [MealRow]
} deriving (Show, Generic)
instance FromJSON MenuGroup

data Pictogram = Pictogram {
    id :: Int,
    name :: Text
} deriving (Show, Generic)
instance FromJSON Pictogram

data CanteenMenu = CanteenMenu {
    groups :: [MenuGroup],
    pictograms :: Maybe [Pictogram]
} deriving (Show, Generic)
instance FromJSON CanteenMenu

baseUrl :: Url 'Https
baseUrl = https "kamweb.ruk.cuni.cz" /: "webkredit"

getCanteens :: Req (Either String [Canteen])
getCanteens =
    let url = baseUrl /: "Api" /: "Ordering" /: "Ordering" in do
        r <- req GET url NoReqBody jsonResponse mempty
        pure $ do
            respJson <-
                maybeToEither "JSON parsing failure"
                (responseBody r :: Maybe Object)
            canteensJson <-
                maybeToEither "Response does not have 'canteens' key"
                $ KeyMap.lookup "canteens" respJson
            parseEither parseJSON canteensJson

getCanteenMenu :: Day -> Int -> Req (Either String CanteenMenu)
getCanteenMenu date canteenId = do
    let url = baseUrl /: "Api" /: "Ordering" /: "Menu"
    -- The backend uses timestamps instead of simple dates and requires
    -- timezone-accurate timestamps for midnight
    -- XXX: NEEDS system time zone to be Europe/Prague
    tz <- liftIO $ localNoonTimeZone date
    let ts = localTimeToUTC tz (LocalTime date midnight)
    let opts = "CanteenId" =: canteenId <> "Dates" =: iso8601Show ts
    r <- req GET url NoReqBody jsonResponse opts
    pure $ maybeToEither "JSON parsing failure"
           (responseBody r :: Maybe CanteenMenu)
