-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2024 David Koňařík
{-# LANGUAGE DeriveGeneric, DuplicateRecordFields, FlexibleContexts,
             ImportQualifiedPost, OverloadedLabels, OverloadedRecordDot,
             OverloadedStrings, QuasiQuotes #-}

module Menzostat.PhotoScraper where

import Control.Exception.Safe
import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.ByteString qualified as BS
import Data.ByteString.Lazy qualified as LBS
import Data.Digest.Pure.SHA
import Data.Function
import Data.List
import Data.Map qualified as M
import Data.Maybe
import Data.Text qualified as T
import Data.Text.Encoding
import Database.Selda
import Network.HTTP.Req
import System.IO
import Text.Printf
import Text.Read qualified as TR
import Text.Regex.PCRE.Heavy
import Text.URI

import Menzostat.Database qualified as D
import Menzostat.Utils

data MealPhoto = MealPhoto {
    mealName :: Text,
    kindId :: Int,
    altId :: Int,
    photoExtension :: Text,
    photoData :: BS.ByteString
}

data ConfigEntry = ConfigEntry {
    nazev :: Text,
    obrazek :: Text,
    popis :: Text
} deriving (Show, Generic)
instance FromJSON ConfigEntry

data ConfigJson = ConfigJson {
    nazev :: String,
    jidla :: M.Map String ConfigEntry
} deriving (Show, Generic)
instance FromJSON ConfigJson

-- There is no way to automatically enumerate all the canteens present in this
-- app and assign them to WebKredit IDs. This manually maintained list does
-- just that.
-- Some canteens are divided into multiple IDs, but have the same menu, hence
-- the list.
-- Commented out canteens exist in the app, but don't give correct data (e.g.
-- stock photos)
canteenNamesToIds :: [(Text, [Int])]
canteenNamesToIds = [
    ("jednota", [1]),
    ("arnost", [5]),
    ("rotlev", [12, 59, 63]),
    ("pravnicka", [7, 8]),
    ("albertov", [61, 64]),
    -- ("htf", [11, 60]),
    -- ("budec", [15]),
    ("sport", [18]),
    ("kajetanka", [21]),
    ("hvezda", [54]),
    ("troja", [29, 27]),
    ("lf-hradec", [38]),
    ("kotel", [35, 37]),
    ("lf-plzen", [52]),
    ("safranek", [40]),
    ("malostranska", [66]) ]
    -- Missing: Hostivař, Lidická, Nový kampus

kindMapping :: [(Text, Int)]
kindMapping = [("POLÉVKA", 2), ("MENU", 3)]

skippedNames :: [Regex]
skippedNames = [[re|Rekrabička|]]

getPhotos :: Text -> Text -> Req [MealPhoto]
getPhotos baseUrl canteen = do
    let configUrl = T.pack $ printf "%s/nabidka/%s/config.json" baseUrl canteen
    configResp <- tryAny $ dget configUrl jsonResponse
    case configResp of
        Left e -> do
            liftIO $ putStrLn $ printf "Failed getting config JSON for '%s': %s"
                                       canteen (show e)
            pure []
        Right cr ->
            let configJson = responseBody cr :: ConfigJson in
            configJson.jidla & M.elems & mapM (runMaybeT . getPhoto baseUrl canteen)
                & fmap catMaybes
  where
    getPhoto :: Text -> Text -> ConfigEntry -> MaybeT Req MealPhoto
    getPhoto baseUrl canteen j = do
        photoResp <- dgetMaybe (baseUrl <> j.obrazek) bsResponse
        detailResp <- dgetMaybe (baseUrl <> j.popis) bsResponse
        let detailLines = responseBody detailResp & decodeUtf8 & T.splitOn "\r\n"
        let firstLine = head detailLines
        let reportSkip =
               liftIO
             $ putStrLn
             $ printf "Skipping meal without match on name: canteen '%s' name '%s'"
                      canteen (head detailLines)

        -- Skip entries matching skippedNames
        when (skippedNames & any (firstLine =~)) mzero

        -- Extract menu kind and altId
        (kindStr, altStr) <-
            case firstLine
                 & scan [re|^ *([^0-9]+)([0-9]*).*$|] of
                [(_, [ks, as])] -> pure (T.strip ks, as)
                _ -> do
                    reportSkip
                    mzero
        -- Match menu kind
        kindId <-
            case kindMapping & find (\(s, _) -> s == T.toUpper kindStr) of
                Just (_, ki) -> pure ki
                Nothing -> do
                    reportSkip
                    mzero
        let altId = fromMaybe 1 $ TR.readMaybe $ T.unpack altStr
        pure $ MealPhoto {
            mealName = detailLines !! 2
                     & sub [re|^[0-9]+g|^CZ |^CS |] ("" :: Text)
                     & T.strip,
            kindId = kindId,
            altId = altId,
            photoExtension = j.obrazek & T.splitOn "." & last,
            photoData = responseBody photoResp
        }
    -- Compile-time safety is nice and all, but it sometimes leads to designs
    -- that have no nice way to do simple tasks, like send a request
    -- to a dynamic url...
    dget urlStr p = do
        u <- mkURI urlStr
        case fromJust $ useURI u of
            Left (http, o) -> req GET http NoReqBody p o
            Right (https, o) -> req GET https NoReqBody p o
    dgetMaybe u p = do
        e <- tryAny (lift $ dget u p)
        let m = eitherToMaybe e
        MaybeT (return m)


savePhotos :: [Int] -> FilePath -> [MealPhoto] -> SeldaM b ()
savePhotos canteenIds photoDir photos = do
    day <- liftIO today
    photos & mapM_ (\p -> do
        let hash = showDigest $ sha256 $ LBS.fromStrict p.photoData
        let name = hash ++ "." ++ T.unpack p.photoExtension
        liftIO $ withFile (photoDir <> name)
                          WriteMode
                          (\h -> BS.hPut h p.photoData)
        count <-
            update D.meals
                   (\m -> foldl (\c i -> c .|| (m ! #canteenId .== literal i))
                                 false canteenIds
                      .&& m ! #day .== literal day
                      .&& m ! #kindId .== literal p.kindId
                      .&& m ! #altId .== literal p.altId)
                      -- TODO: Also check if name matches?
                      -- Sometimes they just slightly don't match, but
                      -- sometimes it indicates desync from webkredit menu.
                   (\m -> m `with` [
                       #photoName := literal (Just $ T.pack name)
                   ])
        when (count == 0) $ do
            liftIO $ putStrLn $ printf "Meal %s,%d,%d:'%s' did not match (photo %s)"
                                       (show canteenIds) p.kindId p.altId
                                       p.mealName name)

runPhotoScraper :: Text -> FilePath -> SeldaM b ()
runPhotoScraper baseUrl photoDir =
    canteenNamesToIds & mapM_ (\(cn, ci) -> do
        photos <- runReq defaultHttpConfig $ getPhotos baseUrl cn
        savePhotos ci photoDir photos)
