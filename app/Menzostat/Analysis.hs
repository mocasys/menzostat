-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2022 David Koňařík
{-# LANGUAGE OverloadedRecordDot #-}
module Menzostat.Analysis where

import Data.List
import Data.Time.Clock
import Data.Time.LocalTime

import Menzostat.Database
import Menzostat.Utils

totalServed :: [MealHistory] -> Int
totalServed histUnsorted =
    let histSorted = sortOn timestamp histUnsorted in
    snd $ foldl step (0, 0) histSorted
  where
    step (lastAvailable, served) hi =
        let available = countAvailable hi in
        (available,
         if available > lastAvailable
         then served
         else served + (lastAvailable - available))

historyItemsByMeal :: [MealHistory] -> [[MealHistory]]
historyItemsByMeal = groupSortOn (\hi -> (hi.canteenId, hi.day, hi.kindId, hi.altId))

lastHistoryItemsByMeal :: [MealHistory] -> [MealHistory]
lastHistoryItemsByMeal = map (last . sortOn (.timestamp)) . historyItemsByMeal

-- For one meal only!
-- The input data is (timestamp, count).
-- Count can either be countAvailable (counting down with purchases)
-- or countOrdered (counting up), the latter has to be multiplied by -1!
servedByStep :: TimeZone -> DiffTime -> [(UTCTime, Int)] -> [Int]
servedByStep tz stepSize histUnsorted =
    let histSorted = sortOn fst histUnsorted
        (_, res) = foldl (step tz stepSize) (0, [(0, 0)]) histSorted in
    aListToList 0 (0, floor ((secondsToDiffTime 24*3600)/stepSize) - 1) res
  where
    step :: TimeZone
         -> DiffTime
         -> (Int, [(Int, Int)])
         -> (UTCTime, Int)
         -> (Int, [(Int, Int)])
    step _ _ (_, []) _ = error "servedByStep ate first entry"
    step tz stepSize
         (lastAvailable, (lastStep, servedLast):prev)
         (timestamp, count) =
        let available = case count of -1 -> 0; a -> a
            localTime = utcToLocalTime tz timestamp
            dt = sinceMidnight $ localTimeOfDay localTime
            step = floor $ dt / stepSize
            served = if available > lastAvailable
                     then 0
                     else lastAvailable - available in
        (available,
         if step == lastStep
         then (step, servedLast + served):prev
         else (step, served):(lastStep, servedLast):prev)
