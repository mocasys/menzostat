-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2022 David Koňařík
{-# LANGUAGE DuplicateRecordFields, FlexibleContexts, ImportQualifiedPost,
             OverloadedLabels, OverloadedRecordDot, OverloadedStrings #-}

module Menzostat.Worker where

import Data.Function
import Data.List ((\\))
import Database.Selda

import Menzostat.Analysis
import Menzostat.Database qualified as D
import Menzostat.Utils

processStats :: Day -> SeldaM b ()
processStats day = do
    liftIO $ putStrLn $ "processStats " ++ show day
    tz <- liftIO $ localNoonTimeZone day
    history <- query $ do
        mh <- select D.mealHistory
        restrict $ mh ! #day .== literal day
        order (mh ! #timestamp) ascending
        order (mh ! #altId) ascending
        order (mh ! #kindId) ascending
        order (mh ! #canteenId) ascending
        pure mh
    let items =
         history
         & groupSortOn (\mh -> (mh.canteenId, mh.kindId, mh.altId))
         & map (\mhs ->
             let h = head mhs
                 hist = map (\mh -> (mh.timestamp, -mh.countOrdered)) mhs in
             D.MealStatistics {
                 canteenId = h.canteenId,
                 day = day,
                 kindId = h.kindId,
                 altId = h.altId,
                 servedByTime = servedByStep tz D.servedByTimeStep hist })
    _ <- insert D.mealStatistics items
    pure ()

processLatest :: Day -> SeldaM b ()
processLatest day = do
    liftIO $ putStrLn $ "processLatest " ++ show day
    history <- query $ do
        mh <- select D.mealHistory
        restrict $ mh ! #day .== literal day
        order (mh ! #timestamp) descending
        order (mh ! #altId) ascending
        order (mh ! #kindId) ascending
        order (mh ! #canteenId) ascending
        pure mh
    _ <- history
        & groupSortOn (\mh -> (mh.canteenId, mh.kindId, mh.altId))
        & mapM (\mhs ->
            let dbItem = head mhs in
            upsert D.mealHistoryLatest
                (\mh -> mh ! #canteenId .== literal dbItem.canteenId
                    .&& mh ! #day .== literal day
                    .&& mh ! #kindId .== literal dbItem.kindId
                    .&& mh ! #altId .== literal dbItem.altId)
                (\mh -> mh `with` [
                    #countAvailable := literal dbItem.countAvailable,
                    #countOrdered := literal dbItem.countOrdered])
                [dbItem])
    pure ()


runWorker :: SeldaM b ()
runWorker = do
    daysAll <- uniqueDays D.meals
    daysStatsDone <- uniqueDays D.mealStatistics
    daysLatestDone <- uniqueDays D.mealHistoryLatest
    t <- liftIO today
    ((daysAll \\ daysStatsDone) \\ [t]) & mapM_ processStats
    (daysAll \\ (daysLatestDone \\ [t])) & mapM_ processLatest
    pure ()
  where
    uniqueDays t = query $ distinct $ do
        x <- select t
        pure $ x ! #day
