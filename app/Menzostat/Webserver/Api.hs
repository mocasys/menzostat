-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2023 David Koňařík
{-# LANGUAGE DuplicateRecordFields, ImportQualifiedPost, OverloadedStrings #-}
module Menzostat.Webserver.Api where

import Codec.Archive.Zip
import Control.Monad
import Control.Monad.IO.Class
import Data.ByteString.Lazy qualified as LBS
import Data.ByteString qualified as BS
import Data.ByteString.Char8 qualified as BS8
import Data.Function
import Data.Text qualified as T
import Data.Text.Encoding
import Data.Text.Lazy qualified as LT
import Data.Time
import Database.PostgreSQL.LibPQ qualified as PQ
import Network.HTTP.Types
import Text.Printf
import Web.Scotty.Trans

import Menzostat.Webserver.Common
import Data.Time.Clock.POSIX (getPOSIXTime)

dbCopyOut :: PQ.Connection -> BS.ByteString -> Action b LBS.ByteString
dbCopyOut conn stmt = do
    mres <- liftIO $ PQ.exec conn stmt
    res <- case mres of
               Just r -> pure r
               Nothing -> raiseStatus internalServerError500 "Failed COPY FROM"
    rs <- liftIO $ PQ.resultStatus res
    when (rs /= PQ.CopyOut)
        $ raiseStatus internalServerError500
                      ("Bad query result: " <> LT.pack (show rs))
    collect [] conn
  where
    collect rowlist conn = do
        cd <- liftIO $ PQ.getCopyData conn False
        case cd of
            PQ.CopyOutError -> do
                em <- liftIO $ PQ.errorMessage conn
                raiseStatus internalServerError500
                            ("Failed COPY FROM: " <> LT.pack (show em))
            PQ.CopyOutDone -> pure (rowlist & reverse & LBS.concat)
            -- Can only happen when getCopyData is called with async=True
            PQ.CopyOutWouldBlock -> fail "Invalid state: CopyOutWouldBlock"
            PQ.CopyOutRow r -> collect (LBS.fromStrict r:rowlist) conn

dbExport :: FilePath -> PageAction b
dbExport dbPath = do
    fromDate <- dayParam "from"
    toDate <- dayParam "to"
    when (diffDays toDate fromDate > 7)
        $ raiseStatus badRequest400 "Only 7 day range is allowed"

    nowFrac <- liftIO getPOSIXTime
    let now = round nowFrac

    conn <- liftIO $ PQ.connectdb $ encodeUtf8 $ T.pack dbPath
    canteens <- dbCopyOut conn "COPY canteens TO STDOUT WITH (FORMAT CSV, HEADER true)"
    let archive1 = emptyArchive & addEntryToArchive (
         toEntry "canteens.csv" now canteens)

    archive2 <-
        ["meals", "mealhistory", "mealhistory_latest",
         "mealstatistics" :: String]
        & foldM (\ar tbl -> do
              let stmt = printf ("COPY (SELECT * FROM %s"
                              <> " WHERE day BETWEEN '%s' AND '%s')"
                              <> " TO STDOUT WITH (FORMAT CSV, HEADER true)")
                                tbl (show fromDate) (show toDate)
                         & BS8.pack
              dat <- dbCopyOut conn stmt
              pure $ addEntryToArchive (toEntry (tbl <> ".csv") now dat) ar)
                archive1

    liftIO $ PQ.finish conn

    setHeader "Content-Type" "application/zip; charset=binary"
    raw $ fromArchive archive2
