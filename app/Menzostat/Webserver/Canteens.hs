-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2023 David Koňařík
{-# LANGUAGE DuplicateRecordFields, ImportQualifiedPost, OverloadedLabels,
             OverloadedRecordDot, OverloadedStrings #-}
module Menzostat.Webserver.Canteens where

import Control.Monad.Trans.Class
import Data.Function
import Data.Text qualified as T
import Data.Time
import Database.Selda as S
import Lucid as H
import Network.HTTP.Types
import Text.Printf
import Web.Scotty.Trans

import Menzostat.Database qualified as D
import Menzostat.Utils
import Menzostat.Webserver.Common

canteenListPage :: PageAction b
canteenListPage = do
    canteens <- lift $ query $ select D.canteens

    lucid $ basePage "Canteen list" $ do
        h1_ "Canteen list"
        ul_ $ do canteens & mapM_ (\c ->
                  li_ $ canteenLink c.id $ toHtml c.name)

canteenPage :: PageAction b
canteenPage = do
    id <- param "id"
    canteens <- lift $ query $ do
        c <- select D.canteens
        restrict $ c ! #id .== S.literal id
        pure c
    canteen <- case canteens of
               [c] -> pure c
               _   -> raiseStatus notFound404 "No such canteen or too many!"

    meals <- lift $ query $ do
        m <- select D.meals
        restrict $ m ! #canteenId .== S.literal id
        ol <- leftJoin
            (\hi -> hi ! #canteenId .== m ! #canteenId
                .&& hi ! #day .== m ! #day
                .&& hi ! #kindId .== m ! #kindId
                .&& hi ! #altId .== m ! #altId)
            (select D.mealHistoryLatest)
        order (m ! #altId) ascending
        order (m ! #kindId) ascending
        order (m ! #day) descending
        pure $ m :*: ol

    today' <- lift $ lift today
    let twoWeeksAgo = addDays (-14) today'
    let servedPerDayPath =
         printf "/canteen/%d/servedPerDay.svg?from=%s&to=%s"
                id (show twoWeeksAgo) (show today') :: String
    let servedPerStepPath =
         printf "/canteen/%d/servedPerStep.svg?from=%s&to=%s&daysOfWeek=12345"
                id (show twoWeeksAgo) (show today') :: String

    let title = printf "Canteen %d: %s" id canteen.name :: String
    lucid $ basePage (toHtml title) $ do
        h1_ $ toHtml canteen.name
        h2_ "Ordered meals (last two weeks)"
        img_ [src_ $ T.pack servedPerDayPath ]
        h2_ "Meals served per interval"
        img_ [src_ $ T.pack servedPerStepPath ]
        h2_ "Served meals"
        wrappedSortableTable $ do
            thead_ $ tr_ $ do
                th_ "Date"
                H.with th_ [title_ "Kind ID"] "Kind"
                H.with th_ [title_ "Alternative ID"] "Alt"
                th_ "Name"
                H.with th_ [title_ "Total count ordered"] "Ord."
            tbody_ $ meals & mapM_ (\(m :*: hi) -> tr_ $ do
                td_ $ H.with span_
                    [class_ $ case toModifiedJulianDay m.day `mod` 2 of
                                  0 -> "hl hl-de"
                                  1 -> "hl hl-do"
                                  _ -> error "Impossible mod result"]
                    $ toHtml $ show m.day
                td_ $ H.with span_
                    [class_ $ case m.kindId of
                                  2 -> "hl hl-k2"
                                  3 -> "hl hl-k3"
                                  _ -> "hl hl-kx"]
                    $ toHtml $ show m.kindId
                td_ $ H.with span_
                    [class_ $ case (m.kindId, m.altId) of
                                  (2,a) | a `elem` [1,2] ->
                                      "hl hl-k2a" <> tshow a
                                  (3,a) | a `elem` [1..8] ->
                                      "hl hl-k3a" <> tshow a
                                  _ -> "hl hl-kx"]
                    $ toHtml $ show m.altId
                td_ $ occurrenceLink m $ toHtml m.name
                td_ $ toHtml $ hi & maybe "" (show . (.countOrdered)))
