-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2023 David Koňařík
{-# LANGUAGE ImportQualifiedPost, OverloadedRecordDot, OverloadedStrings,
             QuasiQuotes #-}
module Menzostat.Webserver.Static where

import Control.Monad.IO.Class
import Data.Text qualified as T
import Data.Time
import Lucid as H
import Text.RawString.QQ

import Menzostat.Webserver.Common
import Menzostat.Utils

homePage :: PageAction b
homePage = do
    lucid $ basePage "Homepage" $ do
        h1_ "Menzostat"
        p_ $ do
            "Menzostat monitors the "
            H.with a_ [href_ "https://kamweb.ruk.cuni.cz/webkredit/Ordering/Menu"]
                      "official web interface"
            " of the canteens of the Charles University in Prague. It aims "
            "to provide useful historical data for students and staff."

apiDocPage :: PageAction b
apiDocPage = do
    toDate <- liftIO today
    let fromDate = addDays (-7) toDate

    lucid $ basePage "Menzostat API" $ do
        toHtmlRaw ([r|
            <p>Menzostat provides an API for downloading the gathered data
            which lets users download weekly CSV exports of the database. You
            can either use the form below, or send <code>GET</code> requests
            directly.</p> <p>Photos are not included in the exports and can be
            downloaded directly. A given photo <code>abc.jpg</code> is
            available at <code>/photo/abc.jpg</code></p>
        |] :: T.Text)

        form_ [action_ "/db.zip", method_ "GET", class_ "api-form"] $ do
            label_ $ do
                "From: "
                input_ [type_ "date", name_ "from",
                        value_ (T.pack $ showGregorian fromDate)]
            label_ $ do
                "To: "
                input_ [type_ "date", name_ "to",
                        value_ (T.pack $ showGregorian toDate)]
            div_ $ input_ [type_ "submit", value_ "Download .ZIP"]

