-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2023 David Koňařík
{-# LANGUAGE ImportQualifiedPost, OverloadedRecordDot, OverloadedStrings,
             QuasiQuotes #-}
module Menzostat.Webserver.Common where

import Data.Text qualified as T
import Data.Text.Lazy qualified as LT
import Database.Selda
import Lucid as H
import Network.HTTP.Types.Status
import Text.Printf
import Text.RawString.QQ
import Web.Scotty.Trans

import Menzostat.Database qualified as D
import Menzostat.Utils

type Action b = ActionT LT.Text (SeldaT b IO)
type PageAction b = Action b ()

basePage :: Monad m => HtmlT m () -> HtmlT m () -> HtmlT m ()
basePage title content = html_ $ do
    head_ $ do
        meta_ [charset_ "utf-8"]
        meta_ [name_ "viewport", content_ "width=device-width, initial-scale=1"]
        title_ ("Menzostat—" <> title)
        link_ [rel_ "stylesheet", href_ "/static/main.css"]
        script_ [src_ "/static/tablesort.min.js"] ("" :: Html ())
        script_ [r|
            document.addEventListener("DOMContentLoaded", function() {
                document.querySelectorAll("table.sortable").forEach(function(t) {
                    Tablesort(t, {});
                });
            });
        |]
    body_ $ do
        nav_ $ ul_ $ do
            li_ $ H.with a_ [href_ "/"] "Menzostat"
            li_ $ H.with a_ [href_ "/meal"] "Meals"
            li_ $ H.with a_ [href_ "/canteen"] "Canteens"

        main_ content

        footer_ $ do
            ul_ $ do
                li_ $ H.with a_ [href_ "https://gitlab.com/dvdkon/menzostat"]
                                "Code"
                li_ $ H.with a_ [href_ "/api"] "API"
                li_ $ H.with a_ [href_ "mailto:dvdkon-menzostat@konarici.cz"]
                                "E-Mail"

lucid :: (ScottyError e, Monad m) => Html a -> ActionT e m ()
lucid c = html $ renderText c

dayParam :: LT.Text -> Action b Day
dayParam name = do
    dayT <- param name `rescue` (\_ ->
        raiseStatus badRequest400 ("Date parameter missing: " <> name))
    case parseDay dayT of
        Just d -> pure d
        Nothing -> raiseStatus badRequest400 "Invalid day format"


wrappedSortableTable :: Monad m => HtmlT m a -> HtmlT m a
wrappedSortableTable b =
    H.with div_ [class_ "table-wrapper"]
    $ H.with table_ [class_ "sortable"] b

mealLink :: Text -> Html () -> Html ()
mealLink name = H.with a_ [href_ $ T.pack $ printf "/meal/%s" name]

occurrenceLink :: D.Meal -> Html () -> Html ()
occurrenceLink meal =
    let path = printf "/meal/%s/%d/%d/%d" (show meal.day)
                                          meal.canteenId
                                          meal.kindId
                                          meal.altId in
    H.with a_ [href_ $ T.pack path]

canteenLink :: Int -> Html () -> Html ()
canteenLink id = H.with a_ [href_ $ T.pack $ printf "/canteen/%d" id]
