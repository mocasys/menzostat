-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2023 David Koňařík
{-# LANGUAGE DuplicateRecordFields, ImportQualifiedPost, OverloadedLabels,
             OverloadedRecordDot, OverloadedStrings #-}
module Menzostat.Webserver.Charts where

import Control.Monad.Trans.Class
import Data.Function
import Data.List
import Data.List.NonEmpty (nonEmpty)
import Data.Maybe
import Data.Text qualified as T
import Data.Time
import Database.Selda as S
import Formatting as F
import Formatting.Time qualified as FT
import Lucid.Svg
import Network.HTTP.Types.Status
import Text.Read qualified as TR
import Web.Scotty.Trans as W

import Menzostat.Database qualified as D
import Menzostat.SvgCharts
import Menzostat.Webserver.Common
import Menzostat.Utils

svgChart :: Svg () -> PageAction b
svgChart c = do
    setHeader "Content-Type" "image/svg+xml"
    raw $ renderBS c

mealOccurrenceChart :: PageAction b
mealOccurrenceChart = do
    day <- dayParam "day"
    canteenId <- param "canteenId"
    kindId <- param "kindId"
    altId <- param "altId"

    historyItems <- lift $ query $ do
        hi <- select D.mealHistory
        restrict $ hi ! #day .== S.literal day
               .&& hi ! #canteenId .== S.literal canteenId
               .&& hi ! #kindId .== S.literal kindId
               .&& hi ! #altId .== S.literal altId
        order (hi ! #timestamp) Desc
        pure hi

    tz <- liftIO $ localNoonTimeZone day
    let maxVal = historyItems
               & map (\hi -> Prelude.max hi.countAvailable hi.countOrdered)
               & nonEmpty
               & fmap (fromIntegral . maximum)
               & fromMaybe 10
    let yTickVals =
         if maxVal < 25 then [0, maxVal]
                        -- Float ranges are weird, so we cast to/from int
                        else [0::Integer,25..floor maxVal] & map fromIntegral
    let xys = combinedXYScale (doubleScale fromHour toHour)
                              (doubleScale 0 maxVal)
    svgChart $ simpleChartBase 800 300 "" $ \ss -> do
        let cs = paddedXYScale 50 30 30 50 $ xys <++> ss
        xAxis cs
        xTicks cs 0 [(i, sformat (F.float%":00") i) | i <- [fromHour..toHour]]
        yTicks cs 0 [(i, sformat F.float i) | i <- yTickVals]

        let hiLine f =
             dPolyLine (historyItems
                       & filter (\hi -> utctDay hi.timestamp == hi.day)
                       & mapMaybe (\hi ->
                           let localTime = utcToLocalTime tz hi.timestamp in
                           if localTime.localDay == hi.day
                           then cs.scale (
                               (localTime.localTimeOfDay & sinceMidnight)
                               / 3600
                               & toRational & fromRational,
                               fromIntegral (f hi))
                           else Nothing))

        hiLine (.countOrdered) [class_ "data1"]
        hiLine (.countAvailable) [class_ "data2"]
  where
    (fromHour, toHour) = (10, 15)

servedPerDayChart :: PageAction b
servedPerDayChart = do
    id <- param "id"
    from <- dayParam "from"
    to <- dayParam "to"

    meals <- lift $ query $ do
        ms <- select D.mealStatistics
        restrict $ ms ! #canteenId .== S.literal id
               .&& ms ! #day .>= S.literal from
               .&& ms ! #day .<= S.literal to
        pure ms

    let orderedPerDay = meals
         & groupSortOn (.day)
         & map (\mss -> ((head mss).day,
                         mss
                       & map (\ms -> ms.servedByTime
                                   & map fromIntegral
                                   & sum)
                       & sum))

    let maxVal = orderedPerDay
               & map snd
               & nonEmpty
               & fmap maximum
               & fromMaybe 10
    let yTickVals = if maxVal < 25 then [0, maxVal] else [0,100..maxVal]
    let xys = combinedXYScale (categoryScale [from..to])
                              (doubleScale 0 maxVal)
    let extraCss = "text.xtick { text-anchor: start; }"
    svgChart $ simpleChartBase 800 400 extraCss $ \ss -> do
        let cs = paddedXYScale 50 40 30 100 $ xys <++> ss
        let ds = paddedXYScale 30 30 0 0 cs
        xAxis cs
        xTicks ds 40 [(d, T.pack $ show d) | d <- [from..to]]
        yTicks cs 0 [(i, sformat F.float i) | i <- yTickVals]

        orderedPerDay & mapM_ (\(day, ordered) ->
            vertBar ds 0 30 day ordered [class_ "data1"])

servedPerStepChart :: PageAction b
servedPerStepChart = do
    id <- param "id"
    from <- dayParam "from"
    to <- dayParam "to"
    dowText <- param "daysOfWeek"
    dowList <- case dowText & T.unpack & mapM (TR.readMaybe . (:[])) of
        Just l -> pure $ map toEnum l
        Nothing -> raiseStatus badRequest400 "Invalid day of week list"

    let days = [from..to] & filter (\d -> dayOfWeek d `elem` dowList)

    meals <- lift $ query $ do
        ms <- select D.mealStatistics
        restrict $ ms ! #canteenId .== S.literal id
               .&& ms ! #day `isIn` map S.literal days
        pure ms

    let days = meals & map (.day) & nub & length
    let sbtSum = meals & map (.servedByTime) & transpose & map (fromIntegral . sum)
    let dat = if not $ null sbtSum
              then sbtSum
                   & drop dropSamples
                   & take takeSamples
                   & map (/ fromIntegral days) -- Make counts per-day
              else timeStepValues D.servedByTimeStep (tmin, tmax) & map (const 0)

    let maxVal' = dat & nonEmpty & fmap maximum & fromMaybe 0
    -- TODO: Fix crash in charts when axis has zero length
    let maxVal = if maxVal' == 0 then 10 else maxVal'
    let xTickVals = timeStepValues D.servedByTimeStep (tmin, tmax)
    let yTickVals =
         if maxVal < 10 then [0, maxVal]
                        else [0::Integer,10..floor maxVal] & map fromIntegral
    let xys = combinedXYScale (categoryScale [0..fromIntegral takeSamples])
                              (doubleScale 0 maxVal)
    let extraCss = "text.xtick { text-anchor: start; }"
    svgChart $ simpleChartBase 800 400 extraCss $ \ss -> do
        let cs = paddedXYScale 50 40 30 70 $ xys <++> ss
        let ds = paddedXYScale 30 30 0 0 cs
        xAxis cs
        xTicks ds 60 [(i, sformat FT.hm h)
                      | (i, h) <- zip [(0::Integer)..] xTickVals]
        yTicks cs 0 [(i, sformat F.float i) | i <- yTickVals]

        dat
         & zip [0..]
         & mapM_ (\(hour, ordered) ->
            vertBar ds 0 20 hour ordered [class_ "data2"])
  where
    (tmin, tmax) = (TimeOfDay 10 0 0, TimeOfDay 15 0 0)
    dropSamples =
        -- -1 to avoid tick for tmin
        length (timeStepValues D.servedByTimeStep (TimeOfDay 0 0 0, tmin)) - 1
    takeSamples = length $ timeStepValues D.servedByTimeStep (tmin, tmax)
