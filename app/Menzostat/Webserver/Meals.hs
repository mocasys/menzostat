-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2023 David Koňařík
{-# LANGUAGE DuplicateRecordFields, ImportQualifiedPost, NamedFieldPuns,
             OverloadedLabels, OverloadedRecordDot, OverloadedStrings #-}
module Menzostat.Webserver.Meals where

import Control.Monad.Trans.Class
import Data.Function
import Data.List qualified as L
import Data.Maybe
import Data.Text qualified as T
import Database.Selda as S
import Lucid as H
import Network.HTTP.Types
import Text.Printf
import Web.Scotty.Trans

import Menzostat.Analysis
import Menzostat.Database qualified as D
import Menzostat.Utils
import Menzostat.Webserver.Common

mealListPage :: PageAction b
mealListPage = do
    meals <- lift $ query $ aggregate $ do
        m <- select D.meals
        n' <- groupBy (m ! #name)
        pure (n' :*: count (m ! #name) :*: S.max_ (m ! #kindId))
    let mealsByKind = groupSortOn (\(_ :*: _ :*: k) -> k) meals
    lucid $ basePage "Meal list" $
        mapM_ (\ms -> do
            let _ :*: _ :*: kindIdM = head ms
            let kindId = fromJust kindIdM
            h2_ $ toHtml (printf "Meals of kind %d" kindId :: String)

            let dms = groupSortOn (\(n :*: _ :*: _) -> n) ms
            wrappedSortableTable $ do
                thead_ $ tr_ $ do
                    th_ "Name"
                    th_ "Occurs"
                    th_ "Days"
                tbody_ $ mapM_ (\dm ->
                    tr_ $ do
                        let n :*: _ :*: _ = head dm
                        let c = sum (map (\(_ :*: c :*: _) -> c) dm)
                        td_ $ mealLink n $ toHtml n
                        td_ $ toHtml $ show c
                        td_ $ toHtml $ show $ length dm
                    ) dms
            ) mealsByKind

mealDetailPage :: PageAction b
mealDetailPage = do
    mealName <- param "name"
    occurs <- lift $ query $ do
        m <- select D.meals
        restrict (m ! #name .== S.literal mealName)
        c <- innerJoin (\c -> c ! #id .== m ! #canteenId)
                       (select D.canteens)
        order (m ! #day) descending
        pure (c ! #name :*: m)
    historyItems' <- lift $ query $ do
        m <- select D.meals
        restrict (m ! #name .== S.literal mealName)
        leftJoin
            (\hi -> hi ! #canteenId .== m ! #canteenId
                .&& hi ! #day .== m ! #day
                .&& hi ! #kindId .== m ! #kindId
                .&& hi ! #altId .== m ! #altId)
            (select D.mealHistoryLatest)
    let historyItems = catMaybes historyItems'

    let title = printf "Meal detail: %s" mealName :: String
    lucid $ basePage (toHtml title) $ do
        h1_ $ toHtml mealName
        p_ $ do
            toHtml (printf "Occurred %d times" (length occurs) :: String)
            br_ []
            let ordered = historyItems
                 & lastHistoryItemsByMeal
                 & map (.countOrdered)
                 & sum
            toHtml (printf "Ordered %d times" ordered :: String)

        h2_ "Served"
        wrappedSortableTable $ do
            thead_ $ tr_ $ do
                th_ "Day"
                th_ "Canteen"
                th_ "No. ordered"
                th_ "No. left"
            tbody_ $ mapM_ (\(canteenName :*: m) ->
                tr_ $ do
                    let D.Meal { canteenId, day, kindId, altId } = m
                    let hs =
                           historyItems
                         & L.find (\hi ->
                             hi.canteenId == canteenId && hi.day == day
                          && hi.kindId == kindId && hi.altId == altId)
                    let noAvailData = any (\hi -> D.countAvailable hi == -1) hs
                    let ordered = case hs of
                                  Just hi -> show $ D.countOrdered hi
                                  Nothing -> ""
                    let left = case hs of
                               Just hi | not noAvailData ->
                                   show $ D.countAvailable hi
                               _ -> ""
                    td_ $ occurrenceLink m $ toHtml $ show m.day
                    td_ $ occurrenceLink m $ toHtml canteenName
                    td_ $ toHtml ordered
                    td_ $ toHtml left
                ) occurs

mealOccurrencePage :: PageAction b
mealOccurrencePage = do
    day <- dayParam "day"
    canteenId <- param "canteenId"
    kindId <- param "kindId"
    altId <- param "altId"

    meals <- lift $ query $ do
        m <- select D.meals
        restrict $ m ! #day .== S.literal day
               .&& m ! #canteenId .== S.literal canteenId
               .&& m ! #kindId .== S.literal kindId
               .&& m ! #altId .== S.literal altId
        c <- innerJoin (\c -> c ! #id .== m ! #canteenId)
                       (select D.canteens)
        pure (c ! #name :*: m)
    canteenName :*: meal <-
        case meals of
            [m] -> pure m
            _   -> raiseStatus notFound404 "No such meal or too many!"

    historyItems <- lift $ query $ do
        hi <- select D.mealHistory
        restrict $ hi ! #day .== S.literal day
               .&& hi ! #canteenId .== S.literal canteenId
               .&& hi ! #kindId .== S.literal kindId
               .&& hi ! #altId .== S.literal altId
        pure hi

    let title = printf "Meal occurrence: %s on %s" meal.name (show day) :: String
    lucid $ basePage (toHtml title) $ do
        h1_ $ mealLink meal.name $ toHtml meal.name
        p_ $ do
            "Served on: "; toHtml (show day); br_ []
            "Canteen: "; canteenLink canteenId $ toHtml canteenName; br_ []
            "Kind ID: "; toHtml $ show meal.kindId; br_ []
            "Alternative ID: "; toHtml $ show meal.altId; br_ []
            let fromCents c = fromIntegral c / 100 :: Float
            toHtml (printf "Price (full/subsidised): %.2f Kč / %.2f Kč"
                           (fromCents meal.price)
                           (fromCents meal.price2)
                           :: String)
            br_ []
            "Served total: "
            toHtml $ show $ (last historyItems).countOrdered; br_ []

        case meal.photoName of
            Just pn -> img_ [src_ $ T.pack $ printf "/photo/%s" pn]
            Nothing -> mempty

        h2_ "History"
        let chartPath = printf "/meal/%s/%d/%d/%d/chart.svg"
                               (show day) canteenId kindId altId :: String
        img_ [src_ $ T.pack chartPath]
