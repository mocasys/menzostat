-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2023 David Koňařík
{-# LANGUAGE ImportQualifiedPost #-}
module Menzostat.Utils where

import Control.Concurrent
import Control.Monad.IO.Class
import Data.Function
import Data.List
import Data.Maybe
import Data.Text qualified as T
import Data.Time.Calendar
import Data.Time.Clock
import Data.Time.Format
import Data.Time.LocalTime
import GHC.Int
import System.Clock

maybeToEither :: a -> Maybe b -> Either a b
maybeToEither _ (Just v) = Right v
maybeToEither n Nothing = Left n

eitherToMaybe :: Either a1 a2 -> Maybe a2
eitherToMaybe (Left _) = Nothing
eitherToMaybe (Right r) = Just r

tshow :: Show a => a -> T.Text
tshow = T.pack . show

groupSortOn :: (Eq b, Ord b) => (a -> b) -> [a] -> [[a]]
groupSortOn acc = groupBy (\x y -> acc x == acc y) . sortOn acc

parseDay :: String -> Maybe Day
parseDay = parseTimeM True defaultTimeLocale "%Y-%m-%d" :: String -> Maybe Day

periodicallyWithState :: MonadIO m => (s -> m s) -> s -> Int64 -> m ()
periodicallyWithState body initialState periodMs = do
    let clock = Monotonic
    let period = normalize $ TimeSpec 0 (periodMs * 1000000)
    let rec nextExecution state = do
         now <- liftIO $ getTime clock
         let delay = max (TimeSpec 0 0) (nextExecution - now)
         let nextNsecMsec = fromIntegral $ nsec delay `div` 1000
         let nextSecMsec = fromIntegral $ sec delay * 1000000
         liftIO $ threadDelay $ nextNsecMsec + nextSecMsec
         newState <- body state
         rec (normalize $ nextExecution + period) newState

    now <- liftIO $ getTime clock
    rec now initialState

periodically :: MonadIO m => m () -> Int64 -> m ()
periodically b = periodicallyWithState (\() -> b) ()

today :: IO Day
today = do
    now <- liftIO getCurrentTime
    let localNow = utcToLocalTime utc now
    pure $ localDay localNow

-- Kind of a hack, works for CET/CEST
localNoonTimeZone :: Day -> IO TimeZone
localNoonTimeZone d = getTimeZone (UTCTime d (12 * 3600))

aListToList :: (Num i, Enum i, Eq i) => a -> (i, i) -> [(i, a)] -> [a]
aListToList def (min, max) dat =
    let xs = [0..(max - min)] in
    xs & map (\x ->
        let ix = min + x in
        dat & find (\(ix', _) -> ix' == ix)
            & fmap snd
            & fromMaybe def)

-- Day is not Num, so we can't simply use the generic verson
datedAListToList :: a -> (Day, Day) -> [(Day, a)] -> [a]
datedAListToList def (minDay, maxDay) dat =
    let xs = [0..(diffDays maxDay minDay)] in
    xs & map (\x ->
        let d = addDays x minDay in
        dat & find (\(d', _) -> d' == d)
            & fmap snd
            & fromMaybe def)

timeStepValues :: DiffTime -> (TimeOfDay, TimeOfDay) -> [TimeOfDay]
timeStepValues step (min, max) =
    let minDT = sinceMidnight min
        maxDT = sinceMidnight max
        diffTimes = [minDT, minDT + step .. maxDT] in
    map pastMidnight diffTimes
