-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2022 David Koňařík
{-# LANGUAGE DuplicateRecordFields, ImportQualifiedPost, OverloadedLabels,
             OverloadedRecordDot, OverloadedStrings, ScopedTypeVariables #-}

module Menzostat.Scraper where

import Control.Monad
import Control.Monad.Catch
import Data.Function
import Data.Maybe
import Data.Scientific
import Data.Text qualified as T
import Data.Time
import Database.Selda
import Network.HTTP.Req qualified as Req
import System.Exit
import Text.Printf

import Menzostat.Database qualified as D
import Menzostat.Utils
import Menzostat.WebkreditClient qualified as W

scrapeCanteens :: Req.Req (SeldaM b [Int])
scrapeCanteens = do
    cse <- W.getCanteens
    case cse of
        Left e -> liftIO $ error e
        Right cs -> pure $ do
            liftIO $ putStrLn $ printf "Found %d canteens" (length cs)
            mapM_ upsertCanteen cs
            pure $ map (\c -> c.id) cs
  where
    upsertCanteen :: W.Canteen -> SeldaM b ()
    upsertCanteen c =
        void $ upsert D.canteens
            (\cr -> cr ! #id .== literal c.id)
            (\cr -> cr `with` [#name := literal c.name])
            [D.Canteen c.id c.name]

scrapeMenu :: [Int] -> Day -> Req.Req (SeldaM b ())
scrapeMenu canteenIds date = do
    timestamp <- liftIO getCurrentTime
    menus <- mapM (\i -> fmap (fmap (\m -> (i, m))) $ W.getCanteenMenu date i) canteenIds
    case sequence menus of
        Left e -> liftIO $ error e
        Right ms -> pure $ do
            let meals = ms
                 & map snd
                 & concatMap (.groups)
                 & map (.rows)
                 & catMaybes
                 & concat
            liftIO $ putStrLn $ printf "Found %d meals" (length meals)
            mapM_ (uncurry (upsertMenu timestamp date)) ms
  where
    upsertMenu :: UTCTime -> Day -> Int -> W.CanteenMenu -> SeldaM b ()
    upsertMenu timestamp date cid cmenu = do
        W.groups cmenu & mapM_ (\mg ->
            fromMaybe [] mg.rows & mapM_ (\mr -> do
                let meal = mr.item
                let mealName = T.strip meal.mealName
                priceInt <- priceToCentsInt meal.price
                price2Int <- priceToCentsInt meal.price2
                let dbMeal = D.Meal {
                    canteenId = cid,
                    day = date,
                    kindId = meal.mealKindId,
                    altId = meal.altId,
                    name = mealName,
                    price = priceInt,
                    price2 = price2Int,
                    allergens = fromMaybe "" meal.allergens,
                    photoName = Nothing }
                _ <- upsert D.meals
                    (\m -> m ! #canteenId .== literal cid
                       .&& m ! #day .== literal dbMeal.day
                       .&& m ! #kindId .== literal meal.mealKindId
                       .&& m ! #altId .== literal meal.altId)
                    (\m -> m `with` [
                        #name := literal mealName,
                        #price := literal priceInt,
                        #price2 := literal price2Int,
                        #allergens := literal dbMeal.allergens])
                    [dbMeal]
                let historyItem = D.MealHistory {
                    canteenId = cid,
                    day = date,
                    kindId = meal.mealKindId,
                    altId = meal.altId,
                    timestamp = timestamp,
                    countAvailable = fromMaybe (-1) meal.countAvailable,
                    countOrdered = meal.countOrdered }
                insert D.mealHistory [historyItem]))
    priceToCentsInt :: Scientific -> SeldaM b Int
    priceToCentsInt p =
        case toBoundedInteger (p * 100) of
            Nothing -> error $ printf "Failed to convert price (%s)"
                                      (show p)
            Just pi -> pure pi

runScraper :: Maybe Day -> Bool -> SeldaM b ()
runScraper setDay oneShot =
    let initialState = ([], ModifiedJulianDay 0) in
    if oneShot then void $ periodic initialState
    else periodicallyWithState periodic initialState 30000
  where
    periodic (oldCanteenIds, lastCanteenUpdate) = do
        today' <- liftIO today
        let day = fromMaybe today' setDay

        canteenIds <-
            if lastCanteenUpdate < day
            then do
                liftIO $ putStrLn "Scraping canteen list..."
                catch (join $ Req.runReq Req.defaultHttpConfig scrapeCanteens)
                       (\(ex :: SomeException) -> liftIO $ do
                           putStr "Error scraping canteen list: "
                           print ex
                           exitFailure)
            else pure oldCanteenIds
        liftIO $ putStrLn "Scraping menus..."
        join $ Req.runReq Req.defaultHttpConfig $ scrapeMenu canteenIds day
        pure (canteenIds, day)
