-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2022 David Koňařík
{-# LANGUAGE DeriveGeneric, DuplicateRecordFields, FlexibleInstances,
             OverloadedLabels, OverloadedStrings #-}

module Menzostat.Database where

import Control.Exception (Exception, throw)
import Data.Function
import Data.Text
import Data.Time.Clock
import Database.Selda
import Database.Selda.SqlType
import Text.Read

newtype SqlCodecException = SqlCodecException String deriving (Show)
instance Exception SqlCodecException

instance SqlType [Int] where
    mkLit xs = LCustom TText (LText (xs & fmap (pack . show) & intercalate ","))
    sqlType _ = TText
    fromSql (SqlString s) =
        case splitOn "," s & mapM (readEither . unpack) of
            Right xs -> xs
            Left e ->
                throw $ SqlCodecException
                    $ "[Int] column with non-int components: " ++ show e
    fromSql v = throw $ SqlCodecException
                    $ "[Int] column with non-string value: " ++ show v
    defaultValue = mkLit []


data Canteen = Canteen {
    id :: Int,
    name :: Text
} deriving (Show, Generic)
instance SqlRow Canteen

canteens :: Table Canteen
canteens = table "canteens" [#id :- primary]

data Meal = Meal {
    canteenId :: Int,
    day :: Day,
    kindId :: Int,
    altId :: Int,
    name :: Text,
    price :: Int, -- Prices in cents
    price2 :: Int,
    allergens :: Text,
    photoName :: Maybe Text
} deriving (Show, Generic)
instance SqlRow Meal

meals :: Table Meal
meals = table "meals" [(#canteenId :+ #day :+ #kindId :+ #altId) :- primary]

data MealHistory = MealHistory {
    canteenId :: Int,
    day :: Day,
    kindId :: Int,
    altId :: Int,
    timestamp :: UTCTime,
    countAvailable :: Int,
    countOrdered :: Int
} deriving (Show, Generic)
instance SqlRow MealHistory

mealHistory :: Table MealHistory
mealHistory =
    table "mealhistory"
          [(#canteenId :+ #day :+ #kindId :+ #altId :+ #timestamp) :- primary]


mealHistoryLatest :: Table MealHistory
mealHistoryLatest =
    table "mealhistory_latest"
          [(#canteenId :+ #day :+ #kindId :+ #altId) :- primary]

data MealStatistics = MealStatistics {
    canteenId :: Int,
    day :: Day,
    kindId :: Int,
    altId :: Int,
    -- A list of how many meals were served in each time step as defined
    -- by servedByTimeStep
    -- Stored by local time (with 15-minute steps, 8th value is 2:00-2:15
    -- by local time on the specific day)
    servedByTime :: [Int]
} deriving (Show, Generic)
instance SqlRow MealStatistics

servedByTimeStep :: DiffTime
servedByTimeStep = secondsToDiffTime 900

mealStatistics :: Table MealStatistics
mealStatistics =
    table "mealstatistics"
          [(#canteenId :+ #day :+ #kindId :+ #altId) :- primary]

createTables :: MonadSelda m => m ()
createTables = do
    createTable canteens
    createTable meals
    createTable mealHistory
    createTable mealHistoryLatest
    createTable mealStatistics
