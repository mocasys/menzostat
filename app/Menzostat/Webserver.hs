-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2023 David Koňařík
{-# LANGUAGE DuplicateRecordFields, ImportQualifiedPost, OverloadedStrings #-}
module Menzostat.Webserver where

import Control.Monad
import Data.Text qualified as T
import Data.Text.Lazy qualified as LT
import Database.Selda as S
import Network.HTTP.Types.Status
import Network.URI.Encode
import Network.Wai
import Web.Scotty.Trans as W

import Menzostat.Webserver.Api
import Menzostat.Webserver.Canteens
import Menzostat.Webserver.Charts
import Menzostat.Webserver.Common
import Menzostat.Webserver.Meals
import Menzostat.Webserver.Static

runServer :: Int
          -> (SeldaT b IO Response -> IO Response)
          -> FilePath
          -> FilePath
          -> IO ()
runServer listenPort dbRunner dbPath photoDir =
    scottyT listenPort dbRunner router
  where
    router :: ScottyT LT.Text (SeldaT b IO) ()
    router = do
        get (regex "^/(static/.*)$") $ do
            path <- param "1"
            let ext = last $ T.splitOn "." path
            setHeader "Content-Type" $ case ext :: Text of
                "css" -> "text/css"
                _ -> "application/octet-stream"
            file $ T.unpack $ T.replace "../" "" path
        get "/db.zip" $ dbExport dbPath
        get "/photo/:name" $ servePhoto photoDir
        get "/" homePage
        get "/api" apiDocPage
        get "/meal" mealListPage
        get "/meal/:day/:canteenId/:kindId/:altId" mealOccurrencePage
        get "/meal/:day/:canteenId/:kindId/:altId/chart.svg" mealOccurrenceChart
        -- Scotty.regex chokes on Unicode, because it uses regex-compat...
        -- It's 2023 people! (╯°□°)╯︵ ┻━┻
        get (function $ \req ->
             let p = decodeBSToText $ rawPathInfo req in
             if T.take 6 p == "/meal/"
             then Just [("name", LT.fromStrict $ T.drop 6 p)]
             else Nothing) mealDetailPage
        get "/canteen" canteenListPage
        get "/canteen/:id" canteenPage
        get "/canteen/:id/servedPerDay.svg" servedPerDayChart
        get "/canteen/:id/servedPerStep.svg" servedPerStepChart

servePhoto :: FilePath -> PageAction b
servePhoto photoDir = do
    name <- param "name"
    when (T.isInfixOf "/" name)
        $ raiseStatus badRequest400 "Invalid photo filename"
    file $ photoDir <> "/" <> T.unpack name
