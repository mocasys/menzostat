-- This file is part of Menzostat and is licenced under the GNU AGPLv3 or later
-- (c) 2023 David Koňařík
{-# LANGUAGE OverloadedStrings, ImportQualifiedPost #-}
module Main where

import Control.Monad.IO.Class
import Data.Maybe
import Data.Text qualified as T
import Data.Time.Format.ISO8601
import Database.Selda.PostgreSQL
import Network.HTTP.Req qualified as Req
import Options.Applicative
import System.IO

import Menzostat.Database qualified as D
import Menzostat.PhotoScraper
import Menzostat.Scraper
import Menzostat.Utils
import Menzostat.WebkreditClient
import Menzostat.Webserver
import Menzostat.Worker
import Data.String (IsString(fromString))

data CliCommand =
      SetupDb
    | RunScraper { setDay :: Maybe String, oneShot :: Bool }
    | RunPhotoScraper { baseUrl :: T.Text, photoDir :: FilePath }
    | RunWorker
    | DebugPrint { day :: String, canteenId :: Int }
    | HttpServer { listenPort :: Int, photoDir :: FilePath }

data CliOptions = CliOptions {
    dbPath :: FilePath,
    cmd :: CliCommand
}

cliParser :: Parser CliOptions
cliParser = CliOptions
    <$> strOption (
           long "db"
        <> short 'd'
        <> metavar "URL"
        <> help "PostgreSQL database URL/connection string")
    <*> cliCommandParser
  where
    cliCommandParser = subparser (
           command "setup-db"
            (info (pure SetupDb)
                  (progDesc "Create database tables"))
        <> command "run-scraper"
            (info runScraperParser
                  (progDesc "Run scraper daemon"))
        <> command "run-photo-scraper"
            (info runPhotoScraperParser
                  (progDesc "Run one-time photo scraper"))
        <> command "run-worker"
            (info runWorkerParser
                  (progDesc "Run worker process to update cache tables"))
        <> command "debug-print" (info debugPrintParser (progDesc ""))
        <> command "http-server" (info httpServerParser (progDesc "")))
    runScraperParser = RunScraper
        <$> optional (strOption (
               long "day"
            <> short 'D'
            <> help "Get data for specific date instead of today"))
        <*> switch (
               long "oneshot"
            <> short 'o'
            <> help "Scrape only once")
    runPhotoScraperParser = RunPhotoScraper
        <$> strOption (
               long "url"
            <> short 'u'
            <> help "Base URL for website")
        <*> strOption (
               long "photodir"
            <> short 'P'
            <> help "Directory for saving photos")
    runWorkerParser = pure RunWorker
    debugPrintParser = DebugPrint
        <$> strOption (long "day" <> short 'D' <> help "Day")
        <*> option auto (long "canteen" <> short 'c' <> help "Canteen ID")
    httpServerParser = HttpServer
        <$> option auto (long "port" <> short 'p')
        <*> strOption (
                long "photodir"
             <> short 'P'
             <> help "Directory for loading photos")

cliParserInfo :: ParserInfo CliOptions
cliParserInfo =
    info (cliParser <**> helper) (
        header "menzostat - Collects and distributes data from CUNI canteens")

main :: IO ()
main = do
    -- Sensible buffering for logs
    hSetBuffering stdout LineBuffering

    opts <- execParser cliParserInfo
    case cmd opts of
        DebugPrint { day = d, canteenId = id } ->
            let day = fromJust $ iso8601ParseM d in
            withReq $ do
                canteens <- getCanteens
                liftIO $ print canteens
                menu <- getCanteenMenu day id
                liftIO $ print menu
                pure ()
        SetupDb -> withDb opts D.createTables
        RunScraper d o ->
            let day = fmap (fromJust . parseDay) d
            in withDb opts $ runScraper day o
        RunPhotoScraper u p ->
            withDb opts $ runPhotoScraper u p
        RunWorker -> withDb opts runWorker
        HttpServer p pd ->
            runServer p (withDb opts) (dbPath opts) pd
  where
    withReq = Req.runReq Req.defaultHttpConfig
    withDb o = withPostgreSQL (fromString (dbPath o))
