# Menzostat

Menzostat is a Haskell-based daemon that collects data from [the official
website](https://kamweb.ruk.cuni.cz/webkredit/) of canteens of the Charles
University in Prague. It records availability hundreds of times per day and
provides a [web-based interface](https://menzostat.ggu.cz/) with tables and
charts derived from the data.

The full database of the public instance is
[available for download](https://menzostat.ggu.cz/db.sqlite3).

# Website description

The website is comprised of multiple pages that are interlinked together:

- `/` Homepage: The homepage serves only as an entrypoint to the website
- `/meal` Meal list: A list of all meals that have been seen by Menzostat,
  grouped by kind (typically 2 -- soup, 3 -- main course). The number besides
  the meal name signifies, how many times this meal has been offered.
- `/meal/MEAL_NAME` Meal detail: Some statistics for the selected meal and a
  table of all the occurrences of the meal, i.e. times the meal has been
  offered by a specific canteen.
- `/meal/DATE/CANTEEN_ID/KIND_ID/ALT_ID` Meal occurrence: Details of a
  particular meal occurrence and a list of all recorded scrapings of that meal
  (i.e. state shown at a particular time).
- `/canteen` Canteen list: A list of all canteens.
- `/canteen/CANTEEN_ID` Canteen detail: Charts and a list of served meals with
  links to occurrences for a particular canteen.  
  The first two charts show how many meals have been ordered/served on
  particular days in the last two weeks. The third chart shows a histogram of
  meals served by hour of day using data from the last two weeks.
- `/api` Data export: A form for downloading part of the database, backed by
  `/db.zip`.

# Installation

To compile and run Menzostat, you'll need:

- [GHC](https://www.haskell.org/ghc/) version 9.2.\*
- [Cabal](https://www.haskell.org/cabal/)
- A PostgreSQL database

To initialise the database, run `cabal run menzostat -- -d <PostgreSQL
connection string> setup-db`. (see [libpq
docs](https://www.postgresql.org/docs/current/libpq-connect.html#LIBPQ-CONNSTRING)).

To run the scraper daemon with its default settings (scrape current day,
periodically), run `cabal run menzostat -- -d <connstr> run-scraper`. You may
also add `-D YYYY-MM-DD` to select the day to scrape and `-o` to run in
one-shot mode.

Separate periodic processes are used for scraping photos (`run-photo-scraper`)
and database maintenance (`run-worker`).

To run the webserver, run `cabal run menzostat -- http-server -p PORT -d
<connstr>`, where `PORT` is the TCP port number the server will listen on.

# Code structure

The code has many dependencies, which are listed in `menzostat.cabal`. Most
notably, it uses:

- [aeson](https://hackage.haskell.org/package/aeson) for JSON parsing
- [lucid](https://hackage.haskell.org/package/lucid) as a DSL for generating
  HTML
- [optparse-applicative](https://hackage.haskell.org/package/optparse-applicative)
  for command-line parsing
- [Scotty](https://hackage.haskell.org/package/scotty) as the web server
- [Selda](https://selda.link/) for accessing the SQLite database

The project itself is divided into multiple modules:

- `Analysis` provides helper functions that analyse the stored data. It is
  mostly used for charts.
- `Database` contains a definition of the database schema and a simple
  migration function (table creation is handled by Selda).
- `Main` processes the command line arguments and launches one of the
  "subprograms".
- `PhotoScraper` contains the photo scraper subprogram, assigning photos to
  existing meals in the database.
- `Scraper` contains the scraper subprogram, which periodically fetches
  current data and saves it in the database.
- `SvgCharts` contains a custom set of low-level functions for creating SVG
  charts.
- `Utils` provides miscellaneous functions used elsewhere in the code.
- `WebkreditClient` contains functions for fetching data from the source
  website and converting it into Haskell types via aeson.
- `Webserver` contains routes for webpages and charts served by the
  `http-server` command.
- `Worker` contains the worker subprogram, periodically populating summary
  tables which serve as a computation cache.
- `Webserver/Api.hs` serves `/db.zip`.
- `Webserver/Canteens.hs` serves `/canteen/*`.
- `Webserver/Common.hs` contains code used in other web modules.
- `Webserver/Charts.hs` serves SVG charts.
- `Webserver/Meals.hs` serves `/meal/*`.
- `Webserver/Static.hs` serves `/api` and the homepage.
